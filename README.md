# ansible-tower-gcp #
This project consists of two major pieces of work:

1. Fully automated configuration of Ansible Tower (in Vagrant) including setting up of:
    * Projects
    * Dynamic Google Compute Inventory and Groups
    * Local and Cloud Credentials
    * Workflows and Surveys

2. Complete playbooks that demonstrate end-to-end provisioning, configuration and testing of a compute instance in Google Cloud Platform using Ansible Tower

## Requirements
* Vagrant - tested in ver 1.9.5
* Ansible - tested in ver 2.3.10
* Ansible Tower Enterprise License - since this project makes use of Workflows and Surveys. A trial license can be requested [here](https://www.ansible.com/license)
* Google Cloud Platform (GCP) accounts - two credentials are needed
   * System Account - needed to connection to GCP API in provisioning instances. Guide on setting up a system account [here](https://support.google.com/cloud/answer/6158849?hl=en&ref_topic=6262490#serviceaccounts)
   * Machine Account - used for connecting to the created instance via SSH. Guide on how to create keys for SSH connection [here](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys)

## Configuration
The Ansible Tower configuration playbooks and files are inside `tower-vagrant/provisioner/` folder. The following vars inside **main.yml** are required to be updated:

Variable | Description
---------|-------------
`tower_license_file` | Full local path/filename of the Tower license file 
`gcp_project_id` | Google Cloud Project ID
`gcp_system_account_name` | System account created in Google Cloud
`gcp_system_account_email` | Email address of system account in Google Cloud
`gcp_system_account_json` | Full local path/filename to Google Cloud system account key (in json format)
`gcp_machine_account_name`| Machine account username created in Google Cloud
`gcp_machine_account_key` | Private key configured for the machine acount in Google Cloud

Inside `group_vars/all/vars.yml` update:
`gce_ssh_user`: Machine account username created in Google Cloud (same as `gcp_machine_account_name` above)

## Running
Once all the necessary files (license, keys) and accounts are saved and defined in the required vars, go inside `tower-vagrant/` directory and run:
`vagrant up`